<!doctype html>
<html lang="pt-br" class="no-js">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <title>
        <g:layoutTitle default="Grails"/>
    </title>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <asset:link rel="icon" href="a-icon.png" type="image/x-png"/>

    <asset:stylesheet src="application.css"/>


    <g:layoutHead/>
</head>

<body>
<nav class="navbar navbar-expand-lg sticky-top navbar-dark bg-dark">
        <div class="container">
            <a class="navbar-brand" href="#">Online Contacts Book</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#conteudoNavbarSuportado" aria-controls="conteudoNavbarSuportado" aria-expanded="false" aria-label="Alterna navegação">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="conteudoNavbarSuportado">
                <ul class="navbar-nav ml-auto">
                    <UIHelper:memberActionMenu/>
                </ul>

        </div>
    </nav>

    <div class="container jumbotron">
        <div class="row">
            <div class="col-3">
                <div class="list-group">
                    <UIHelper:leftNavigation/>
                  </div>
            </div>
            <div class="col-9">
                <g:layoutBody/>
            </div>
        </div>
    </div>

<asset:javascript src="application.js"/>

<script type="text/javascript">
    APP.baseURL = "${UIHelper.appBaseURL()}";
    <g:if test="${flash?.message && flash?.message?.info}">
    jQuery(document).ready(function () {
        APP.messageBox.showMessage(Boolean(${flash.message?.success}), "${flash.message?.info}");
    });
    </g:if>
</script>
%{-- --}%

</body>
</html>
