<!doctype html>
<html lang="pt-br" class="no-js">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <title>
        <g:layoutTitle default="Grails"/>
    </title>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <asset:link rel="icon" href="a-icon.png" type="image/x-png"/>

    <asset:stylesheet src="application.css"/>


    <g:layoutHead/>
</head>

<body>
<nav class="navbar sticky-top navbar-dark bg-dark">
        <div class="container">
            <a class="navbar-brand" href="#">Online Contacts Book</a>
        </div>
    </nav>

    <div class="container jumbotron mt-4">
        <div class="row">
            <div class="col-12">
                <g:layoutBody/>
            </div>
        </div>
    </div>

    <div class="jumbotron jumbotron-fluid my-0 py-5">
        <div class="container">
            <div class="row">
                <div class="col-sm-4">
                    <h3>OCB</h3>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
                </div>
                <div class="col-sm-4">
                    <h3> Menu</h3>
                    <div class="list-group">
                        <a class="list-group-item list-group-item-action list-group-item-dark bg-gradient-dark active" href ="#">Página Inicial</a>
                        <a class="list-group-item list-group-item-action list-group-item-dark" href ="#">Quem somos</a>
                        <a class="list-group-item list-group-item-action list-group-item-dark" href ="#">Contatos</a>
                    </div>
                </div>
                <div class="col-sm-4">
                    <h3> Redes Sociais </h3>
                    <div class="btn-group-vertical btn-block btn-group-lg" role="droup">
                        <a class="btn btn-dark " href ="#"> <i class="fab fa-facebook"></i> Facebook</a>
                        <a class="btn btn-dark " href ="#"> <i class="fab fa-twitter-square"></i> Twitter</a>
                        <a class="btn btn-dark " href ="#"> <i class="fab fa-instagram"></i> Instagram</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

<asset:javascript src="application.js"/>

<script type="text/javascript">
    <g:if test="${flash?.message && flash?.message?.info}">
    jQuery(document).ready(function () {
        APP.messageBox.showMessage(Boolean(${flash.message?.success}), "${flash.message?.info}");
    });
    </g:if>
</script>
%{-- --}%

</body>
</html>
