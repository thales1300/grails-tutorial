<meta name="layout" content="public"/>

<div class="container">
    <div class="row">
        <div class="col-sm-6 col-md-4 mx-auto">
            <g:img dir="images" file="book.png" class="img-fluid w-50 mx-auto d-block"/>
            <h1 class="text-center login-title">Contacts Book Member</h1>
            <g:form controller="authentication" action="doLogin" class="form-signin">
                <g:textField name="email" class="form-control my-2" placeholder="Email" required="required" />
                <g:passwordField name="password" class="form-control my-2" placeholder="Password" required="required" />
                <g:submitButton class="btn btn-lg btn-dark btn-block my-2" name="login" value="Login"/>
                <g:link controller="authentication" action="registration" class="btn btn-lg btn-dark btn-block">Member Registration</g:link>
            </g:form>
        </div>
    </div>
</div>
