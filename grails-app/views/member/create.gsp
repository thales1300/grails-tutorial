<meta name="layout" content="main"/>

<div class="card">
    <div class="card-header">
        <g:message code="member" args="['Create']"/>
    </div>
    <div class="card-body">
        <g:form controller="member" action="save">

            <g:render template="form"/> %{-- Rendereiza o form--}%
            <div class="form-action-panel">
                <g:submitButton class="btn btn-dark" name="save" value="${g.message(code: "save")}"/> %{-- chama o controller save--}%
                <g:link controller="member" action="index" class="btn btn-dark"><g:message code="cancel"/></g:link>
                %{-- Ao ser clickado seta o controller e a action, Code = mensagem--}%
            </div>
        </g:form>
    </div>
</div>