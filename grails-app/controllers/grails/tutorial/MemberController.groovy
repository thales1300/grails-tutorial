package grails.tutorial

class MemberController {

    MemberService memberService

    def index() { //Retorna a lista de usuarios ao index
        def response = memberService.list(params)
        [memberList: response.list, total:response.count]
    }

    def details(Integer id) { //Redireciona para a pag response em caso de sucesso.
        def response = memberService.getById(id)
        if (!response){
            redirect(controller: "member", action: "index")
        }else{
            [member: response]
        }
    }

    def create() { //Redireciona os parametros para salvar
        [member: flash.redirectParams]
    }

    def save() { //Faz o chamado da função save
        def response = memberService.save(params)
        if (!response.isSuccess) {
            flash.redirectParams = response.model
            flash.message = AppUtil.infoMessage(g.message(code: "unable.to.save"), false)
            redirect(controller: "member", action: "create")
        }else{
            flash.message = AppUtil.infoMessage(g.message(code: "saved"))
            redirect(controller: "member", action: "index")
        }
    }


    def edit(Integer id) { //Redireciona a pagina para o formulario de update
        if (flash.redirectParams) {
            [member: flash.redirectParams]
        } else {
            def response = memberService.getById(id)
            if (!response) {
                flash.message = AppUtil.infoMessage(g.message(code: "invalid.entity"), false)
                redirect(controller: "member", action: "index")
            } else {
                [member: response]
            }
        }
    }


    def update() { //chama a função de update
        def response = memberService.getById(params.id)
        if (!response){
            flash.message = AppUtil.infoMessage(g.message(code: "invalid.entity"), false)
            redirect(controller: "member", action: "index")
        }else{
            response = memberService.update(response, params)
            if (!response.isSuccess){
                flash.redirectParams = response.model
                flash.message = AppUtil.infoMessage(g.message(code: "unable.to.update"), false)
                redirect(controller: "member", action: "edit")
            }else{
                flash.message = AppUtil.infoMessage(g.message(code: "updated"))
                redirect(controller: "member", action: "index")
            }
        }
    }

    def delete(Integer id) { //Chama a função de delete
        def response = memberService.getById(id)

        if (!response){
            flash.message = AppUtil.infoMessage(g.message(code: "invalid.entity"), false)
            redirect(controller: "member", action: "index")
        }else{
            response = memberService.delete(response)
            if (!response){
                flash.message = AppUtil.infoMessage(g.message(code: "unable.to.delete"), false)
            }else{
                flash.message = AppUtil.infoMessage(g.message(code: "deleted"))
            }
            redirect(controller: "member", action: "index")
        }
    }
}
