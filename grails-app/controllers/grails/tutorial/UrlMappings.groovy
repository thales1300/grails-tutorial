package grails.tutorial

class UrlMappings {

    static mappings = {
        "/$controller/$action?/$id?(.$format)?"{
            constraints {
                // apply constraints here
            }
        }

        "/"(controller: "dashboard", action: "index") //seta  index para dashboard-index
        "500"(view:'/error')
        "404"(view:'/notFound')
    }
}
