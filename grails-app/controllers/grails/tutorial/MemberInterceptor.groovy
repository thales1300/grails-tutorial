package grails.tutorial


class MemberInterceptor {

    AuthenticationService authenticationService

    boolean before() { //intercepta caso o mebro seja admistrador ou não e faz o retorno
        if (authenticationService.isAdministratorMember()){
            return true
        }
        flash.message = AppUtil.infoMessage(g.message(code: "member.not.adm"), false)
        redirect(controller: "dashboard", action: "index")
        return false
    }
}
