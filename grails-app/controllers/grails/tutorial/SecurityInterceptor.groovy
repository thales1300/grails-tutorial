package grails.tutorial


class SecurityInterceptor {

    AuthenticationService authenticationService

    SecurityInterceptor() { //controla tudo menos a autenticação
        matchAll().excludes(controller: "authentication")
    }

    boolean before() { //caso não esteja autenticado redireciona para a pagina de login
        if (!authenticationService.isAuthenticated()) {
            redirect(controller: "authentication", action: "login")
            return false
        }
        return true
    }
}
