package grails.tutorial

class AuthenticationController {

    AuthenticationService authenticationService
    MemberService memberService

    def login() { //faz o redirecionamento para a pagina de login
        if (authenticationService.isAuthenticated()) {
            redirect(controller: "dashboard", action: "index")
        }
    }

    def doLogin() { //faz a auteicação do login, caso true, vai para sua dashboard, caso false, retorna ao login
        if (authenticationService.doLogin(params.email, params.password)) {
            redirect(controller: "dashboard", action: "index")
        } else {
            flash.message = AppUtil.infoMessage(g.message(code: "error.login"), false)
            redirect(controller: "authentication", action: "login")
        }
    }

    def logout() { //faz logout da sessão
        session.invalidate()
        redirect(controller: "authentication", action: "login")
    }

    def registration() { //faz o redireceionamento dos parametros para registrar
        [member: flash.redirectParams]
    }


    def doRegistration() { //realiza o registro de membro
        def response = memberService.save(params)
        if (response.isSuccess) {
            authenticationService.setMemberAuthorization(response.model)
            redirect(controller: "dashboard", action: "index")
        } else {
            flash.redirectParams = response.model
            redirect(controller: "authentication", action: "registration")
        }
    }
}
