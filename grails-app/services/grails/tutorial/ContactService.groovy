package grails.tutorial

import grails.gorm.transactions.Transactional
import grails.web.servlet.mvc.GrailsParameterMap
import javax.servlet.http.HttpServletRequest

@Transactional
class ContactService {

    AuthenticationService authenticationService
    ContactDetailsService contactDetailsService

    def save(GrailsParameterMap params, HttpServletRequest request) {
        Contact contact = new Contact(params)
        contact.member = authenticationService.getMember()
        def response = AppUtil.saveResponse(false, contact)
        if (contact.validate()) {
            contact.save(flush: true)
            if (!contact.hasErrors()){
                response.isSuccess = true
                contactDetailsService.createOrUpdateDetails(contact, params)
                uploadImage(contact, request)
            }
        }
        return response
    }

    def update(Contact contact, GrailsParameterMap params, HttpServletRequest request) {
        contact.properties = params
        def response = AppUtil.saveResponse(false, contact)
        if (contact.validate()) {
            contact.save(flush: true)
            if (!contact.hasErrors()){
                response.isSuccess = true
                contactDetailsService.createOrUpdateDetails(contact, params)
                uploadImage(contact, request)
            }
        }
        return response
    }


    def get(Serializable id) {
        return Contact.get(id)
    }


    def list(GrailsParameterMap params) {
        params.max = params.max ?: GlobalConfig.itemsPerPage()
        List<Contact> contactList = Contact.createCriteria().list(params) {
            if (params?.colName && params?.colValue) {
                like(params.colName, "%" + params.colValue + "%")
            }
            if (!params.sort) {
                order("id", "desc")
            }
            eq("member", authenticationService.getMember())
        }
        return [list: contactList, count: contactList.totalCount]
    }


    def delete(Contact contact) {
        try {
            imageDelete(contact)
            contact.delete(flush: true)
        } catch (Exception e) {
            println(e.getMessage())
            return false
        }
        return true
    }

    def uploadImage(Contact contact, HttpServletRequest request){
        if (request.getFile("contactImage") && !request.getFile("contactImage").filename.equals("")){ //testa se o arquivo existe e seu nome não é nulo
            String image = FileUtil.uploadContactImage(contact.id, request.getFile("contactImage")) //chama função de criar o contato
            if (!image.equals("")){
                contact.image = image
                contact.save(flush:true)
            }
        }
    }

    def imageDelete(def contac){
        String delete = FileUtil.getRootPath()+"contact-image/"+contac.id + "-" +contac.image
        File file = new File(delete)
        if (file.exists()){
            file.delete()
        }
    }
}
