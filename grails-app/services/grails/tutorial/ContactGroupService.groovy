package grails.tutorial

import grails.gorm.transactions.Transactional
import grails.web.servlet.mvc.GrailsParameterMap

@Transactional
class ContactGroupService {
    //mesmo principio do MEMBER - sem comentarios
    AuthenticationService authenticationService

    def save(def params) {
        ContactGroup contactGroup = new ContactGroup(params)
        contactGroup.member = authenticationService.getMember()
        def response = AppUtil.saveResponse(false, contactGroup)
        if (contactGroup.validate()) {
            response.isSuccess = true
            contactGroup.save()
        }
        return response
    }

    def update(ContactGroup contactGroup, GrailsParameterMap params) {
        contactGroup.properties = params
        def response = AppUtil.saveResponse(false, contactGroup)
        if (contactGroup.validate()) {
            response.isSuccess = true
            contactGroup.save(flush:true)
        }
        return response
    }

    def get(Serializable id) {
        return ContactGroup.get(id)
    }

    def list(GrailsParameterMap params) {
        params.max = params.max?: GlobalConfig.itemsPerPage()
        List<ContactGroup> contactGroupList = ContactGroup.createCriteria().list(params) {
            if (params?.colName && params?.colValue){
                like(params.colName, "%" +  params.colValue + "%")
            }
            if (!params.sort){
                order("id","desc")
            }
            eq("member", authenticationService.getMember()) //testa se o membro é o mesmo da sessão
        }
        return [list:contactGroupList, count:contactGroupList.totalCount]
    }


    def getGroupList(){
        return ContactGroup.createCriteria().list {
            eq("member", authenticationService.getMember())
        }
    }

    def cleanGroupContactById(Integer id){ //delete um contato do grupo
        ContactGroup contactGroup = ContactGroup.get(id)
        contactGroup.contact.each {contact ->
            contact.removeFromContactGroup(contactGroup)
        }
        contactGroup.save(flush:true)
    }


    def delete(ContactGroup contactGroup) { // deletar o grupo.
        try {
            cleanGroupContactById(contactGroup.id)
            contactGroup.delete(flush: true)
        } catch (Exception e) {
            println(e.getMessage())
            return false
        }
        return true
    }
}
