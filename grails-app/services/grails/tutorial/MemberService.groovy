package grails.tutorial

import grails.web.servlet.mvc.GrailsParameterMap
import grails.gorm.transactions.Transactional
//Atualizado,mudanças no hibernate se faz necesario do uso de interações Transactional em uma versão mais recente do grails.
@Transactional
class MemberService {

    def save(GrailsParameterMap params) { //salva os dados em uma list
        Member member = new Member(params)
        def response = AppUtil.saveResponse(false, member)
        if (member.validate()) { //, realizado o teste no salvamento.
            if (!member.save().hasErrors()){
                response.isSuccess = true
            }
        }
        return response
    }


    def update(Member member, GrailsParameterMap params) { // update um membro da lista
        member.properties = params
        def response = AppUtil.saveResponse(false, member)
        if (member.validate()) {
            if (!member.save().hasErrors()){
                response.isSuccess = true
            }
        }
        return response
    }


    def getById(Serializable id) { //pega os dados do BD pelo seu ID
        return Member.get(id)
    }


    def list(GrailsParameterMap params) { //Recuepra a lista de membros
        params.max = params.max ?: GlobalConfig.itemsPerPage()
        List<Member> memberList = Member.createCriteria().list(params) {
            if (params?.colName && params?.colValue) {
                like(params.colName, "%" + params.colValue + "%")
            }
            if (!params.sort) {
                order("id", "desc")
            }
        }
        return [list: memberList, count: memberList.totalCount]
    }


    def delete(Member member) { //deleta um membro da lista
        try {
            member.delete(flush: true)
        } catch (Exception e) {
            println(e.getMessage())
            return false
        }
        return true
    }
}
