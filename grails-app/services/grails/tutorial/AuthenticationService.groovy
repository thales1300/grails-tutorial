package grails.tutorial

import grails.gorm.transactions.Transactional

@Transactional
class AuthenticationService {

    private static final String AUTHORIZED = "AUTHORIZED"

    def setMemberAuthorization(Member member) { //Faz a autorização da sessaão
        def authorization = [isLoggedIn: true, member: member]
        AppUtil.getAppSession()[AUTHORIZED] = authorization
    }

    def doLogin(String email, String password){ //Faz a busca pelo email e senha do usuario e faz o retorno, caso true seta autorização ao membro.
        password = password.encodeAsMD5()
        Member member = Member.findByEmailAndPassword(email, password)
        if (member){
            setMemberAuthorization(member)
            return true
        }
        return false
    }

    boolean isAuthenticated(){ //faz a autenticação do login
        def authorization = AppUtil.getAppSession()[AUTHORIZED]
        if (authorization && authorization.isLoggedIn){
            return true
        }
        return false
    }


    def getMember(){ //retorna o membro autorizado
        def authorization = AppUtil.getAppSession()[AUTHORIZED]
        return authorization?.member
    }


    def getMemberName(){ //retorna o nome do membro
        def member = getMember()
        return "${member.firstName} ${member.lastName}"
    }
    def isAdministratorMember(){ //faz a checagem se o member é admistador
        def member = getMember()
        if (member && member.memberType == GlobalConfig.USER_TYPE.ADMINISTRATOR){
            return true
        }
        return false
    }
}
