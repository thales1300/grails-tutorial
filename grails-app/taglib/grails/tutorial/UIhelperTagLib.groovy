package grails.tutorial

class UIhelperTagLib {
    static namespace = "UIHelper" //define a tag a ser usada

    AuthenticationService authenticationService //utilizado para pegar o nome da sessão
    ContactGroupService contactGroupService //Chamado da calsse para utilziar getGroupList()

    //Em caso de erro na validação ele gera um subtite informando a mensagem de erro.
    def renderErrorMessage = { attrs, body ->
        def model = attrs.model
        String fieldName = attrs.fieldName
        String errorMessage = attrs.errorMessage? g.message(code: attrs.errorMessage): g.message(code: "invalid.input")
        if (model && model.errors && model.errors.getFieldError(fieldName)){
            out << "<small class='form-text text-danger''><strong>${errorMessage}</strong></small>"
        }
    }

    //Gera um Dropdown com as opções do usuario
    def memberActionMenu = { attrs, body ->
        out << '<li class="nav-item dropdown show">'
        out << g.link(class:"nav-link dropdown-toggle", "data-toggle":"dropdown"){authenticationService.getMemberName()}
        out << '<div class="dropdown-menu">'
        out << g.link(controller: "authentication", action: "logout", class: "dropdown-item"){g.message(code:"logout")}
        out << "</div></li>"
    }

    //gera o menu laretal com as actions possiveis
    def leftNavigation = { attrs, body ->
        List navigations = [
                [controller: "dashboard", action: "index", name: "dashboard"],
                [controller: "contactGroup", action: "index", name: "contact.group"],
                [controller: "contact", action: "index", name: "contact"],
        ]

        if(authenticationService.isAdministratorMember()){ //caso seja adm mostra a aopção membro
            navigations.add([controller: "member", action: "index", name: "member"])
        }

        navigations.each { menu ->
            out << g.link(controller: menu.controller, action: menu.action, class: "list-group-item list-group-item-action list-group-item-dark") { g.message(code: menu.name, args: ['']) }
        }
    }

    //Mostra a lista de contactGropus e seleciona a qual o contato irá pertencer
    def contactGroup = { attrs, body ->
        String name = attrs.name ?: "contactGroup"
        out << g.select(class:"form-control", multiple: "multiple", optionValue: "name", optionKey: "id", value: attrs.value, name: name, from: contactGroupService.getGroupList())
    }

    //
    def contactType = { attrs, body ->
        String name = attrs.name ?: "type"
        String value = attrs.value ?: ""
        def select = [:]
        select.HOME = "Home"
        select.PERSONAL = "Personal"
        select.OTHER = "Other"
        out << g.select(from: select, name: name, optionKey: "key", optionValue: "value", value: value, class:"form-control")
    }
    //mostra o estado da URL
    def appBaseURL = { attrs, body ->
        out << AppUtil.baseURL();
    }
}
