package grails.tutorial

class ContactGroup {

    Integer id
    String name
    Member member

    Date dateCreated
    Date lastUpdated
    //Um ContactGroup pode ter varios contatos.
    static belongsTo = Contact
    static hasMany = [contact: Contact]

    static constraints = {
        name(blank: false, nullable: false)
        member(nullable: true)
    }

    static mapping = {
        version(false)
    }
}
