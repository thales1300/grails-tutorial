package grails.tutorial

class Member {
    //elementos da classe
    Integer id
    String firstName //por que é definida como necessarioa porém não está no construtor?
    String lastName
    String email
    String password
    String memberType = GlobalConfig.USER_TYPE.MEMBER
    String identityHash
    Long identityHashLastUpdate
    Boolean isActive = true

    Date dateCreated
    Date lastUpdated

    static constraints = { // construtor da classe
        email(email: true, nullable: false, unique: true, blank: false) //E-mail- unico, não nulo e sem blank atribuido
        password(blank: false) //not blank
        firstName(blank: false)
        lastName(nullable: true) //null
        identityHash(nullable: true)
        identityHashLastUpdate(nullable: true)
    }

    def beforeInsert (){ //Insersão do pass posteriormente
        this.password = this.password.encodeAsMD5()
    }
    def beforeUpdate(){ //Uptade pass posteriormente
        this.password = this.password.encodeAsMD5()
    }

}
