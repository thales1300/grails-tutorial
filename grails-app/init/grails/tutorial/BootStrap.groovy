package grails.tutorial

class BootStrap {

    def init = { servletContext ->
        AppInitializationService.initialize() //Chama o contrutor de AppInitializationService
    }
    def destroy = {
    }
}
