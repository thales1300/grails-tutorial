package grails.tutorial

import org.grails.web.util.WebUtils

class AppUtil {
    //usado para receber respostas da seção
    static saveResponse(Boolean isSuccess, def model) {
        return [isSuccess: isSuccess, model: model]
    }

    static getAppSession() {
        return WebUtils.retrieveGrailsWebRequest().session
    }

    static infoMessage(String message, boolean status = true) {
        return [info: message, success: status]
    }
    //Retorna o estado da URL
    static String baseURL(){
        return "${getAppSession().getServletContext().getContextPath()}/"
    }
}
