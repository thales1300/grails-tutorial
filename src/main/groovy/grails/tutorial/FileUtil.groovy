package grails.tutorial

import grails.util.Holders
import org.springframework.web.multipart.MultipartFile

class FileUtil {

    public static String getRootPath(){
        return Holders.servletContext?.getRealPath("")
    }
    //Faz a criação do arquivo, no diretorio recebido
    public static File makeDirectory(String path){
        File file = new File(path)
        if (!file.exists()){
            file.mkdirs()
        }
        return file
    }

    //Gerando e armazenando o arquivo
    public static String uploadContactImage(Integer contactId, MultipartFile multipartFile){
        if (contactId && multipartFile){
            String contactImagePath = "${getRootPath()}contact-image/"
            makeDirectory(contactImagePath) //onde está a imagem
            multipartFile.transferTo(new File(contactImagePath, contactId + "-" + multipartFile.originalFilename)) // criação
            return multipartFile.originalFilename
        }
        return ""
    }
}
