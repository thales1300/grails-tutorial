package grails.tutorial

class GlobalConfig {
    public  static final def USER_TYPE =[ //Tipos de Usuarios.
            ADMINISTRATOR: "ADMINISTRATOR",
            MEMBER: "MEMBER",
    ]
    public static Integer itemsPerPage() { //Numero de itens por pagina ?
        return 5
    }
}
